package com.example.hafidz.toohard;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hafidz.toohard.databinding.ActivityStartScreenBinding;

public class start_screen extends AppCompatActivity {
    ActivityStartScreenBinding mBinding;
    private Animation animFadeIn;
    private Animation animFadeOut;
    SharedPreferences sharedpreferences;
    public static int totalTap;
    //falsekeun nu ieu
    public static boolean isTrue;
    public static int level;
    public static final String MyPREFERENCES = "MyPrefs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_start_screen);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        animFadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        animFadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        permission();


        animating();
        initiating_onClick_listener();
    }

    private void hide_some_stuff() {
        mBinding.imageView.setVisibility(View.GONE);
        mBinding.imageView2.setVisibility(View.GONE);
        mBinding.textView2Continue.setVisibility(View.GONE);
        mBinding.textView3Level.setVisibility(View.GONE);

    }

    @Override
    protected void onResume() {
        super.onResume();
        levelSelector();
    }

    @Override
    protected void onStart() {
        super.onStart();
        levelSelector();
    }

    private void initiating_onClick_listener() {
        mBinding.button27Play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (level == 0 || level == 1) {
                    startActivity(new Intent(start_screen.this, Level1Notification.class));
//                    startActivity(new Intent(start_screen.this, Level10Splash.class));
                } else if (level == 2) {
                    startActivity(new Intent(start_screen.this, Level2Splash.class));
                } else if (level == 3) {
                    startActivity(new Intent(start_screen.this, Level3Splash.class));
                } else if (level == 4) {
                    startActivity(new Intent(start_screen.this, Level4Splash.class));
                } else if (level == 5) {
                    startActivity(new Intent(start_screen.this, Level5Splash.class));
                } else if (level == 6) {
                    startActivity(new Intent(start_screen.this, Level6Splash.class));
                } else if (level == 7) {
                    startActivity(new Intent(start_screen.this, Level7Splash.class));
                } else if (level == 8) {
                    startActivity(new Intent(start_screen.this, Level8Splash.class));
                } else if (level == 9) {
                    startActivity(new Intent(start_screen.this, Level9Splash.class));
                } else if (level == 10) {
                    startActivity(new Intent(start_screen.this, Level10Splash.class));
                }
            }
        });
        mBinding.button28Setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(start_screen.this, SettingActivity.class));
                overridePendingTransition(0, 0);
            }
        });
        mBinding.button29About.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(start_screen.this, AboutActivity.class));
                overridePendingTransition(0, 0);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Toast.makeText(this, "yay", Toast.LENGTH_SHORT).show();
                }
        }
    }

    private void animating() {

        mBinding.textViewTooHard.startAnimation(animFadeIn);
        mBinding.textView2Continue.startAnimation(animFadeIn);
        mBinding.textView3Level.startAnimation(animFadeIn);
        mBinding.button27Play.startAnimation(animFadeIn);
        mBinding.button28Setting.startAnimation(animFadeIn);
        mBinding.button29About.startAnimation(animFadeIn);
        mBinding.imageView.startAnimation(animFadeIn);
        mBinding.imageView2.startAnimation(animFadeIn);

    }

    private void permission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA},
                1);
    }


    public void levelSelector() {
        level = sharedpreferences.getInt("lastlevel", 1);
        final int finallevel = sharedpreferences.getInt("lastlevel", 1);

        if (level == 1) {
            hide_some_stuff();
        } else {
            mBinding.textView3Level.setText(level + "");
            mBinding.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mBinding.textView3Level.getText().equals(finallevel + "")) {
                        mBinding.textView3Level.setText(level + "");
                    } else {
                        level++;
                        mBinding.textView3Level.setText(level + "");
                    }
                }
            });
            mBinding.imageView2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Integer.parseInt(mBinding.textView3Level.getText().toString()) == 1) {
                        mBinding.textView3Level.setText(level + "");
                    } else {
                        level--;
                        mBinding.textView3Level.setText(level + "");
                    }
                }
            });
        }
    }

}
