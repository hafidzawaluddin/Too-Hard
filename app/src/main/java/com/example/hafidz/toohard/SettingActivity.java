package com.example.hafidz.toohard;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class SettingActivity extends AppCompatActivity {

    ImageView reset, music, back;
    int mute;
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_setting);
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        mute = sharedpreferences.getInt("mute", 0);

        reset = findViewById(R.id.erase_progress);
        back = findViewById(R.id.Mback);
        music = findViewById(R.id.Mute);
        if (mute == 0) {
            music.setImageDrawable(getResources().getDrawable(R.drawable.ic_volume_up_black_24dp));
        } else {
            music.setImageDrawable(getResources().getDrawable(R.drawable.ic_volume_mute_black_24dp));

        }

        reset.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(SettingActivity.this);
            builder.setTitle("Confirm");
            builder.setMessage("Are you sure to clear level?");
            builder.setPositiveButton("YES", (dialog, which) -> {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putInt("lastlevel", 1);
                editor.commit();
                Toast.makeText(SettingActivity.this, "Cleared!", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            });

            builder.setNegativeButton("NO", (dialog, which) -> dialog.dismiss());

            AlertDialog alert = builder.create();
            alert.show();
        });

        music.setOnClickListener(view -> {
            mute = sharedpreferences.getInt("mute", 0);
            if (mute == 0) {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putInt("mute", 1);
                editor.commit();
                music.setImageDrawable(getResources().getDrawable(R.drawable.ic_volume_mute_black_24dp));
            } else {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putInt("mute", 0);
                editor.commit();
                music.setImageDrawable(getResources().getDrawable(R.drawable.ic_volume_up_black_24dp));
            }
        });

        back.setOnClickListener(view -> {
            finish();
            overridePendingTransition(0, 0);
        });
    }
}
