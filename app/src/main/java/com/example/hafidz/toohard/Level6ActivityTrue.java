package com.example.hafidz.toohard;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;

import com.example.hafidz.toohard.databinding.ActivityLevel6TrueBinding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Level6ActivityTrue extends AppCompatActivity {

    ActivityLevel6TrueBinding mBinding;
    int benar;
    SharedPreferences sharedpreferences;
    boolean btn1, btn2, btn3, btn4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_level6_true);
        hide_and_fullscreen();

        Random ran = new Random();
        benar = ran.nextInt(4) + 1;
        Log.d("benar", "" + benar);

        switch (benar) {
            case 1:
                btn1 = true;
                mBinding.TrueLv6Btn1.setImageResource(R.drawable.door_percaya_diri);
                randomButton(1);
                break;
            case 2:
                btn2 = true;
                mBinding.TrueLv6Btn2.setImageResource(R.drawable.door_percaya_diri);
                randomButton(2);
                break;
            case 3:
                btn3 = true;
                mBinding.TrueLv6Btn3.setImageResource(R.drawable.door_percaya_diri);
                randomButton(3);
                break;
            case 4:
                btn4 = true;
                mBinding.TrueLv6Btn4.setImageResource(R.drawable.door_percaya_diri);
                randomButton(4);
                break;
        }

        clikListener();
    }

    void benar() {
        try {
            Level6Activity.lv6.finish();
            Level6ActivityFake.l6af.finish();
            this.finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        int level = sharedpreferences.getInt("lastlevel", 1);
        if (level <= 6) {
            editor.putInt("lastlevel", 7);
            editor.commit();
        }
        startActivity(new Intent(this, Level7Splash.class));
        finish();
        overridePendingTransition(0,0);
    }

    void salah() {
        startActivity(new Intent(this, Level6ActivityTrue.class));
        overridePendingTransition(0,0);
    }

    void clikListener() {
        mBinding.TrueLv6Btn1.setOnClickListener((v) -> {
            if (btn1) {
                benar();
            } else {
                salah();
            }
        });

        mBinding.TrueLv6Btn2.setOnClickListener((v) -> {
            if (btn2) {
                benar();
            } else {
                salah();
            }
        });

        mBinding.TrueLv6Btn3.setOnClickListener((v) -> {
            if (btn3) {
                benar();
            } else {
                salah();
            }
        });

        mBinding.TrueLv6Btn4.setOnClickListener((v) -> {
            if (btn4) {
                benar();
            } else {
                salah();
            }
        });
    }

    private void hide_and_fullscreen() {
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void randomButton(int i){
        ArrayList<Integer> mDoorList = new ArrayList<>();
        mDoorList.add(0, R.drawable.door_berani);
        mDoorList.add(1, R.drawable.door_kuning);
        mDoorList.add(2, R.drawable.door_ungu);
        Collections.shuffle(mDoorList);


        switch (i) {
            case 1:
                mBinding.TrueLv6Btn2.setImageResource(mDoorList.get(0));
                mBinding.TrueLv6Btn3.setImageResource(mDoorList.get(1));
                mBinding.TrueLv6Btn4.setImageResource(mDoorList.get(2));
                break;
            case 2:
                mBinding.TrueLv6Btn1.setImageResource(mDoorList.get(0));
                mBinding.TrueLv6Btn3.setImageResource(mDoorList.get(1));
                mBinding.TrueLv6Btn4.setImageResource(mDoorList.get(2));
                break;
            case 3:
                mBinding.TrueLv6Btn1.setImageResource(mDoorList.get(0));
                mBinding.TrueLv6Btn2.setImageResource(mDoorList.get(1));
                mBinding.TrueLv6Btn4.setImageResource(mDoorList.get(2));
                break;
            case 4:
                mBinding.TrueLv6Btn1.setImageResource(mDoorList.get(0));
                mBinding.TrueLv6Btn2.setImageResource(mDoorList.get(1));
                mBinding.TrueLv6Btn3.setImageResource(mDoorList.get(2));
                break;
        }

    }

}
