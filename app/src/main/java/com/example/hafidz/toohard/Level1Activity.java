package com.example.hafidz.toohard;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import de.hdodenhof.circleimageview.CircleImageView;

public class Level1Activity extends AppCompatActivity {

    int tap = 0;
    TextView textView;
    CircleImageView cirlce;
    RelativeLayout relativeLayout;
    private int time = 3000;
    private final String TAG = this.getClass().getName();
    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_level1);

        hide_and_fullscreen();

        cirlce = findViewById(R.id.profile_image);
        textView = findViewById(R.id.txtview);
        relativeLayout = findViewById(R.id.relative);

        final Handler mHandler = new Handler();
        final Runnable runnable = () -> {
            sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            int level = sharedpreferences.getInt("lastlevel", 1);
            if (level <= 2) {
                editor.putInt("lastlevel", 2);
                editor.commit();
            }
            startActivity(new Intent(Level1Activity.this, Level2Splash.class));
//            startActivity(new Intent(Level1Activity.this, Level10Splash.class));
            finish();
        };

        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tap++;
                start_screen.totalTap++;
                Log.wtf( "onClick: ", start_screen.totalTap+"");
                if (start_screen.totalTap >= 50){
                    start_screen.isTrue = true;
                    Log.wtf("onClick: ","mantap");
                }

                try {
                    if (tap == 1) {
                        relativeLayout.setBackgroundColor(Color.parseColor("#4CAF50"));
                    } else if (tap == 2) {
                        relativeLayout.setBackgroundColor(Color.parseColor("#FFEB3B"));
                    } else if (tap == 3) {
                        relativeLayout.setBackgroundColor(Color.parseColor("#2196F3"));
                    } else if (tap == 4) {
                        relativeLayout.setBackgroundColor(Color.parseColor("#F44336"));
                    } else if (tap == 5) {
                        relativeLayout.setBackgroundColor(Color.parseColor("#000000"));
                        mHandler.postDelayed(runnable, time);
                    } else if (tap > 5) {
                        relativeLayout.setBackgroundColor(Color.parseColor("#ffffff"));
                        tap = 0;
                        mHandler.removeCallbacks(runnable);
                    }
                } catch (Exception e) {
                    Toast.makeText(Level1Activity.this, "" + e, Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onClick: " + e);
                }
            }
        });

    }

    private void hide_and_fullscreen() {
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

}
