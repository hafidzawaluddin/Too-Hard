package com.example.hafidz.toohard.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.hafidz.toohard.Level3Model;
import com.example.hafidz.toohard.Level4Activity;
import com.example.hafidz.toohard.Level4Splash;
import com.example.hafidz.toohard.R;

import java.util.List;

public class Level3Adapter extends RecyclerView.Adapter<Level3Adapter.Level3ViewHolder>{

    private Context mContext;
    private List<Level3Model> mList;
    SharedPreferences sharedpreferences;

    public Level3Adapter(Context mContext, List<Level3Model> mList, SharedPreferences sharedpreferences) {
        this.mContext = mContext;
        this.mList = mList;
        this.sharedpreferences = sharedpreferences;
    }

    @Override
    public Level3ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.level3_item_layout, parent,false);
        return new Level3ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Level3ViewHolder holder, final int position) {
        holder.btn.setText("");
        holder.btn.setBackgroundColor(Color.parseColor(mList.get(position).getText()));
        holder.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mList.get(position).isTrue()){
                    //implemen
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    int level = sharedpreferences.getInt("lastlevel", 1);
                    if (level <= 4) {
                        editor.putInt("lastlevel", 4);
                        editor.commit();
                    }
                    mContext.startActivity(new Intent(mContext, Level4Splash.class));
                    ((Activity)mContext).finish();
                    ((Activity)mContext).overridePendingTransition(0, 0);

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class Level3ViewHolder extends RecyclerView.ViewHolder{
        Button btn;
        Level3ViewHolder(View itemView) {
            super(itemView);
            btn = itemView.findViewById(R.id.level3_item_btn);
        }
    }
}
