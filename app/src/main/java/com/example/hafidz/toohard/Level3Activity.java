package com.example.hafidz.toohard;

import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.WindowManager;

import com.example.hafidz.toohard.Adapter.Level3Adapter;
import com.example.hafidz.toohard.databinding.ActivityLevel3Binding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Level3Activity extends AppCompatActivity{

    ActivityLevel3Binding mBinding;
    List<Level3Model> stringList = new ArrayList<>();
    Level3Adapter mAdapter;
    Handler mHandler = new Handler();
    SharedPreferences sharedpreferences;
    int delay = 800;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hide_and_fullscreen();
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_level3);

        sharedpreferences = getSharedPreferences("MyPrefs",MODE_PRIVATE);
        mAdapter = new Level3Adapter(this, stringList, sharedpreferences);
        mBinding.level3Recyler.setAdapter(mAdapter);
        mBinding.level3Recyler.setLayoutManager(new GridLayoutManager(this,4));

        for(int i = 0; i < 34; i++){
            Level3Model model = new Level3Model("#ffffff", false);
            stringList.add(model);
        }

        Level3Model model1 = new Level3Model("#fffff7", false);
        stringList.add(model1);
        Level3Model model2 = new Level3Model("#fffff7", true);
        stringList.add(model2);

        Collections.shuffle(stringList);
        mAdapter.notifyDataSetChanged();
        startShuffle();
    }

    private void hide_and_fullscreen() {
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try{
                Collections.shuffle(stringList);
                mAdapter.notifyDataSetChanged();
            }finally {
                mHandler.postDelayed(runnable,delay);
            }
        }
    };

    void startShuffle(){
        runnable.run();
    }

    void stopSuffle(){
        mHandler.removeCallbacks(runnable);
    }
}
