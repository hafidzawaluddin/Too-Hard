package com.example.hafidz.toohard;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.hanks.htextview.fall.FallTextView;

public class Level4Splash extends AppCompatActivity {

    FallTextView level, desc;
    private static int time = 4000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_level4_splash);

        level = (FallTextView) findViewById(R.id.tv_level);
        desc = (FallTextView) findViewById(R.id.tv_desc);

        level.setSoundEffectsEnabled(false);
        level.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((FallTextView)view).animateText("Level 4");
            }
        });

        desc.setSoundEffectsEnabled(false);
        desc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((FallTextView)view).animateText("Pick!");
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(Level4Splash.this, Level4Activity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                finish();
                overridePendingTransition(0, 0);
            }
        }, time);
        Handler handler = new Handler();
        Runnable mRunnable = new Runnable() {
            @Override
            public void run() {
                level.performClick();
            }
        };

//1000 di delay 1 detik
        handler.postDelayed(mRunnable, 500);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                desc.performClick();
            }
        },2000);
    }
}
