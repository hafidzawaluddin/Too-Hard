package com.example.hafidz.toohard;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

public class Level2Activity extends AppCompatActivity {

    SharedPreferences sharedpreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level2);
        hide_and_fullscreen();
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        int level = sharedpreferences.getInt("lastlevel", 1);
        if (level <= 3) {
            editor.putInt("lastlevel", 3);
            editor.commit();
        }
       startActivity(new Intent(Level2Activity.this, Level3Splash.class));
       finish();
        overridePendingTransition(0, 0);

        super.onConfigurationChanged(newConfig);
    }

    private void hide_and_fullscreen() {
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

}
