package com.example.hafidz.toohard;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.icu.util.TimeUnit;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;

import com.hanks.htextview.base.HTextView;
import com.hanks.htextview.evaporate.EvaporateTextView;
import com.hanks.htextview.typer.TyperTextView;

import java.io.IOException;

public class SplashActivity extends Activity {
    EvaporateTextView bt;
    TyperTextView present;
    MediaPlayer mediaPlayer;
    private static int time = 4000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        bt = (EvaporateTextView) findViewById(R.id.sans);
        present = (TyperTextView) findViewById(R.id.present);

        bt.setSoundEffectsEnabled(false);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((EvaporateTextView)view).animateText("STUDIO");
            }
        });

        present.setSoundEffectsEnabled(false);
        present.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((TyperTextView)view).animateText("PRESENT");
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, start_screen.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                overridePendingTransition(0, 0);
                finish();

            }
        }, time);
        Handler handler = new Handler();
        Runnable mRunnable = new Runnable() {
            @Override
            public void run() {
                ring();
                bt.performClick();
            }
        };

//1000 di delay 1 detik
        handler.postDelayed(mRunnable, 1500);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                present.performClick();
            }
        },2500);
    }
    public void ring(){
        try {
            Resources resources = getResources();
            AssetFileDescriptor assetFileDescriptor = resources.openRawResourceFd(R.raw.intro);
            mediaPlayer = MediaPlayer.create(this, R.raw.intro);
            mediaPlayer.reset();
            mediaPlayer.setDataSource(assetFileDescriptor.getFileDescriptor(), assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength());
            mediaPlayer.setLooping(false);
            mediaPlayer.prepare();
            mediaPlayer.start();
//            mediaPlayer.
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
