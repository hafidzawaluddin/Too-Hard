package com.example.hafidz.toohard;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.hafidz.toohard.databinding.ActivityLevel10Soal2Binding;

import java.util.ArrayList;
import java.util.List;

public class Level10Soal2Activity extends AppCompatActivity {

    private Level10Model mModel;
    private ActivityLevel10Soal2Binding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_level10_soal2);

        mModel = Level10Activity.mSoal.get(1);
        setLevel2();
        mBinding.lv10Soal2BtnTrue.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    mBinding.lv10Soal2BtnTrue.setBackgroundColor(Color.BLACK);
                    mBinding.lv10Soal2BtnTrue.setTextColor(Color.WHITE);
                }
                else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    mBinding.lv10Soal2BtnTrue.setBackgroundColor(Color.WHITE);
                    mBinding.lv10Soal2BtnTrue.setTextColor(Color.BLACK);
                    mBinding.lv10Soal2BtnTrue.callOnClick();
                }
                return true;
            }
        });
        mBinding.lv10Soal2BtnFalse.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    mBinding.lv10Soal2BtnFalse.setBackgroundColor(Color.BLACK);
                    mBinding.lv10Soal2BtnFalse.setTextColor(Color.WHITE);

                }
                else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    mBinding.lv10Soal2BtnFalse.setBackgroundColor(Color.WHITE);
                    mBinding.lv10Soal2BtnFalse.setTextColor(Color.BLACK);
                    mBinding.lv10Soal2BtnFalse.callOnClick();
                }
                return true;
            }
        });
    }

    private void setLevel2() {

        if (start_screen.isTrue) {
            mBinding.lv10Soal2BtnTrue.setEnabled(true);
            mBinding.lv10Soal2BtnFalse.setEnabled(true);
        } else {
            mBinding.lv10Soal2BtnTrue.setEnabled(false);
            mBinding.lv10Soal2BtnFalse.setEnabled(false);
        }

        mBinding.lv10Soal2TxtSoal2.setText(mModel.getSoal());
        Log.wtf("setLevel2: ", mModel.getSoal());

        if (mModel.isTrue()) {
            mBinding.lv10Soal2BtnTrue.setOnClickListener(view -> benar());
            mBinding.lv10Soal2BtnFalse.setOnClickListener(view -> salah());
        } else {
            mBinding.lv10Soal2BtnTrue.setOnClickListener(view -> salah());
            mBinding.lv10Soal2BtnFalse.setOnClickListener(view -> benar());
        }
    }

    private void benar() {
        Toast.makeText(this, "Correct", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this, Level10Soal3Activity.class));
        finish();
    }

    private void salah() {
//        Toast.makeText(this, "Reset Hapus Shared + Intent", Toast.LENGTH_SHORT).show();

        //Hapus SharedPreferer nya
        startActivity(new Intent(this, ResetActivity10.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Level10Activity.mActivity.finish();
    }
}
