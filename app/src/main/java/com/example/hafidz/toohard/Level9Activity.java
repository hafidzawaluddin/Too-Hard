package com.example.hafidz.toohard;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.Toast;

import com.example.hafidz.toohard.databinding.ActivityLevel9Binding;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class Level9Activity extends AppCompatActivity {

    private ActivityLevel9Binding mBinding;
    private ArrayList<String> mAbjadT = new ArrayList<>();
    private ArrayList<String> mAbjadO = new ArrayList<>();
    private ArrayList<String> mAbjadO1 = new ArrayList<>();
    private ArrayList<String> mAbjadH = new ArrayList<>();
    private ArrayList<String> mAbjadA = new ArrayList<>();
    private ArrayList<String> mAbjadR = new ArrayList<>();
    private ArrayList<String> mAbjadD = new ArrayList<>();
    private String total, T, O, O1, H, A, mR, D;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_level9);
        hide_and_fullscreen();

        setmAbjadT();
        setmAbjadO();
        setmAbjadO1();
        setmAbjadH();
        setmAbjadA();
        setmAbjadR();
        setmAbjadD();

        T = "*";
        O = "*";
        O1 = "*";
        H = "*";
        A = "*";
        mR = "*";
        D = "*";

        total = T + O + O1 + " " + H + A + mR + D;
        mBinding.lv9TxtLevel.setText(total);
        setMaxSeekBar();
    }

    private void setMaxSeekBar() {

        mBinding.lv9SeekT.setMax(103);
        mBinding.lv9SeekT.setProgress(0);

        mBinding.lv9SeekT.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                T = mAbjadT.get(i);
                setText();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mBinding.lv9SeekO.setMax(103);
        mBinding.lv9SeekO.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                O1 = mAbjadO1.get(i);
                setText();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mBinding.lv9SeekO1.setMax(103);
        mBinding.lv9SeekO1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                O = mAbjadO.get(i);
                setText();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mBinding.lv9SeekH.setMax(100);
        mBinding.lv9SeekH.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                H = mAbjadH.get(i);
                setText();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mBinding.lv9SeekA.setMax(100);
        mBinding.lv9SeekA.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                A = mAbjadA.get(i);
                setText();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mBinding.lv9SeekR.setMax(103);
        mBinding.lv9SeekR.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                mR = mAbjadR.get(i);
                setText();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mBinding.lv9SeekD.setMax(100);
        mBinding.lv9SeekD.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                D = mAbjadD.get(i);
                setText();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void setmAbjadT() {
        mAbjadT.add(0, "A");
        mAbjadT.add(1, "B");
        mAbjadT.add(2, "C");
        mAbjadT.add(3, "D");
        mAbjadT.add(4, "E");
        mAbjadT.add(5, "F");
        mAbjadT.add(6, "G");
        mAbjadT.add(7, "H");
        mAbjadT.add(8, "I");
        mAbjadT.add(9, "J");
        mAbjadT.add(10, "L");
        mAbjadT.add(11, "M");
        mAbjadT.add(12, "N");
        mAbjadT.add(13, "O");
        mAbjadT.add(14, "P");
        mAbjadT.add(15, "Q");
        mAbjadT.add(16, "R");
        mAbjadT.add(17, "S");
        mAbjadT.add(18, "U");
        mAbjadT.add(19, "V");
        mAbjadT.add(20, "W");
        mAbjadT.add(21, "X");
        mAbjadT.add(22, "Y");
        mAbjadT.add(23, "Z");

        mAbjadT.addAll(mAbjadT);
        mAbjadT.addAll(mAbjadT);
//        mAbjadT.addAll(mAbjadT);

        mAbjadT.add("T");
        mAbjadT.add("A");
        mAbjadT.add("B");
        mAbjadT.add("C");
        mAbjadT.add("D");
        mAbjadT.add("E");
        mAbjadT.add("F");
        mAbjadT.add("G");
        Log.wtf("setArrayT: ", mAbjadT.size() + "");

        Collections.shuffle(mAbjadT);
    }

    private void setmAbjadO() {

        mAbjadO.add(0, "A");
        mAbjadO.add(1, "B");
        mAbjadO.add(2, "C");
        mAbjadO.add(3, "D");
        mAbjadO.add(4, "E");
        mAbjadO.add(5, "F");
        mAbjadO.add(6, "G");
        mAbjadO.add(7, "H");
        mAbjadO.add(8, "I");
        mAbjadO.add(9, "J");
        mAbjadO.add(10, "L");
        mAbjadO.add(11, "M");
        mAbjadO.add(12, "N");
        mAbjadO.add(13, "P");
        mAbjadO.add(14, "Q");
        mAbjadO.add(15, "R");
        mAbjadO.add(16, "S");
        mAbjadO.add(17, "T");
        mAbjadO.add(18, "U");
        mAbjadO.add(19, "V");
        mAbjadO.add(20, "W");
        mAbjadO.add(21, "X");
        mAbjadO.add(22, "Y");
        mAbjadO.add(23, "Z");

        mAbjadO.addAll(mAbjadO);
        mAbjadO.addAll(mAbjadO);
//        mAbjadO.addAll(mAbjadO);

        mAbjadO.add("O");
        mAbjadO.add("A");
        mAbjadO.add("B");
        mAbjadO.add("C");
        mAbjadO.add("D");
        mAbjadO.add("E");
        mAbjadO.add("F");
        mAbjadO.add("G");

        Log.wtf("setmAbjadO: ", "" + mAbjadO.size());
        Collections.shuffle(mAbjadO);
    }

    private void setmAbjadO1() {
        mAbjadO1.add(0, "A");
        mAbjadO1.add(1, "B");
        mAbjadO1.add(2, "C");
        mAbjadO1.add(3, "D");
        mAbjadO1.add(4, "E");
        mAbjadO1.add(5, "F");
        mAbjadO1.add(6, "G");
        mAbjadO1.add(7, "H");
        mAbjadO1.add(8, "I");
        mAbjadO1.add(9, "J");
        mAbjadO1.add(10, "L");
        mAbjadO1.add(11, "M");
        mAbjadO1.add(12, "N");
        mAbjadO1.add(13, "P");
        mAbjadO1.add(14, "Q");
        mAbjadO1.add(15, "R");
        mAbjadO1.add(16, "S");
        mAbjadO1.add(17, "T");
        mAbjadO1.add(18, "U");
        mAbjadO1.add(19, "V");
        mAbjadO1.add(20, "W");
        mAbjadO1.add(21, "X");
        mAbjadO1.add(22, "Y");
        mAbjadO1.add(23, "Z");

        mAbjadO1.addAll(mAbjadO1);
        mAbjadO1.addAll(mAbjadO1);
//        mAbjadO1.addAll(mAbjadO1);

        mAbjadO1.add("O");
        mAbjadO1.add("A");
        mAbjadO1.add("B");
        mAbjadO1.add("C");
        mAbjadO1.add("D");
        mAbjadO1.add("E");
        mAbjadO1.add("F");
        mAbjadO1.add("G");

        Log.wtf("setArrayO1: ", mAbjadT.size() + "");
        Collections.shuffle(mAbjadO1);
    }

    private void setmAbjadH() {
        mAbjadH.add(0, "A");
        mAbjadH.add(1, "B");
        mAbjadH.add(2, "C");
        mAbjadH.add(3, "D");
        mAbjadH.add(4, "E");
        mAbjadH.add(5, "F");
        mAbjadH.add(6, "G");
        mAbjadH.add(7, "I");
        mAbjadH.add(8, "J");
        mAbjadH.add(9, "K");
        mAbjadH.add(10, "L");
        mAbjadH.add(11, "M");
        mAbjadH.add(12, "N");
        mAbjadH.add(13, "O");
        mAbjadH.add(14, "P");
        mAbjadH.add(15, "Q");
        mAbjadH.add(16, "R");
        mAbjadH.add(17, "S");
        mAbjadH.add(18, "T");
        mAbjadH.add(19, "U");
        mAbjadH.add(20, "V");
        mAbjadH.add(21, "W");
        mAbjadH.add(22, "X");
        mAbjadH.add(23, "Y");
        mAbjadH.add(24, "Z");

        mAbjadH.addAll(mAbjadH);
        mAbjadH.addAll(mAbjadH);
//        mAbjadH.addAll(mAbjadH);

        mAbjadH.add("H");

        Log.wtf("setAbjadH: ", "" + mAbjadH.size());
        Collections.shuffle(mAbjadH);
    }

    private void setmAbjadA() {
        mAbjadA.add(0, "B");
        mAbjadA.add(1, "C");
        mAbjadA.add(2, "D");
        mAbjadA.add(3, "E");
        mAbjadA.add(4, "F");
        mAbjadA.add(5, "G");
        mAbjadA.add(6, "H");
        mAbjadA.add(7, "I");
        mAbjadA.add(8, "J");
        mAbjadA.add(9, "K");
        mAbjadA.add(10, "L");
        mAbjadA.add(11, "M");
        mAbjadA.add(12, "N");
        mAbjadA.add(13, "O");
        mAbjadA.add(14, "P");
        mAbjadA.add(15, "Q");
        mAbjadA.add(16, "R");
        mAbjadA.add(17, "S");
        mAbjadA.add(18, "T");
        mAbjadA.add(19, "U");
        mAbjadA.add(20, "V");
        mAbjadA.add(21, "W");
        mAbjadA.add(22, "X");
        mAbjadA.add(23, "Y");
        mAbjadA.add(24, "Z");

        mAbjadA.addAll(mAbjadA);
        mAbjadA.addAll(mAbjadA);
//        mAbjadA.addAll(mAbjadA);

        mAbjadA.add("A");

        Log.wtf("setAbjadA: ", "" + mAbjadA.size());
        Collections.shuffle(mAbjadA);
    }

    private void setmAbjadR() {

        mAbjadR.add(0, "A");
        mAbjadR.add(1, "B");
        mAbjadR.add(2, "C");
        mAbjadR.add(3, "D");
        mAbjadR.add(4, "E");
        mAbjadR.add(5, "F");
        mAbjadR.add(6, "G");
        mAbjadR.add(7, "I");
        mAbjadR.add(8, "J");
        mAbjadR.add(9, "K");
        mAbjadR.add(10, "L");
        mAbjadR.add(11, "M");
        mAbjadR.add(12, "N");
        mAbjadR.add(13, "O");
        mAbjadR.add(14, "P");
        mAbjadR.add(15, "Q");
        mAbjadR.add(16, "S");
        mAbjadR.add(17, "T");
        mAbjadR.add(18, "U");
        mAbjadR.add(19, "V");
        mAbjadR.add(20, "W");
        mAbjadR.add(21, "X");
        mAbjadR.add(22, "Y");
        mAbjadR.add(23, "Z");

        mAbjadR.addAll(mAbjadR);
        mAbjadR.addAll(mAbjadR);
//        mAbjadR.addAll(mAbjadR);

        mAbjadR.add("R");
        mAbjadR.add("A");
        mAbjadR.add("B");
        mAbjadR.add("C");
        mAbjadR.add("D");
        mAbjadR.add("E");
        mAbjadR.add("F");
        mAbjadR.add("G");

        Log.wtf("setAbjadR: ", "" + mAbjadR.size());
        Collections.shuffle(mAbjadR);

    }

    private void setmAbjadD() {
        mAbjadD.add(0, "A");
        mAbjadD.add(1, "B");
        mAbjadD.add(2, "C");
        mAbjadD.add(3, "D");
        mAbjadD.add(4, "E");
        mAbjadD.add(5, "F");
        mAbjadD.add(6, "G");
        mAbjadD.add(7, "I");
        mAbjadD.add(8, "J");
        mAbjadD.add(9, "K");
        mAbjadD.add(10, "L");
        mAbjadD.add(11, "M");
        mAbjadD.add(12, "N");
        mAbjadD.add(13, "O");
        mAbjadD.add(14, "P");
        mAbjadD.add(15, "Q");
        mAbjadD.add(16, "R");
        mAbjadD.add(17, "S");
        mAbjadD.add(18, "T");
        mAbjadD.add(19, "U");
        mAbjadD.add(20, "V");
        mAbjadD.add(21, "W");
        mAbjadD.add(22, "X");
        mAbjadD.add(23, "Y");
        mAbjadD.add(24, "Z");

        mAbjadD.addAll(mAbjadD);
        mAbjadD.addAll(mAbjadD);
//        mAbjadD.addAll(mAbjadD);

        mAbjadD.add("D");

        Log.wtf("setAbjadD: ", "" + mAbjadD.size());
        Collections.shuffle(mAbjadD);
    }

    private void setText() {
        total = T + O + O1 + " " + H + A + mR + D;
        mBinding.lv9TxtLevel.setText(total);

        if (total.toLowerCase().equals("too hard")) {
            SharedPreferences sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            int level = sharedpreferences.getInt("lastlevel", 1);
            if (level <= 9) {
                editor.putInt("lastlevel", 10);
                editor.commit();
            }
            startActivity(new Intent(this, Level10Splash.class));
            overridePendingTransition(0,0);
        }
    }

    private void hide_and_fullscreen() {
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}
