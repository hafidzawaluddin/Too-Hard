package com.example.hafidz.toohard;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.hafidz.toohard.databinding.ActivityLevel10Binding;
import com.example.hafidz.toohard.databinding.ActivityLevel9Binding;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.logging.Level;

public class Level10Activity extends AppCompatActivity implements View.OnClickListener {

    private ActivityLevel10Binding mBinding;
    public static ArrayList<Level10Model> mSoal = new ArrayList<>();
    private Level10Model mModel;
    public static Activity mActivity;
    private boolean btn1, btn2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_level10);
        mActivity = this;
        setQuiz();
        setupLevel1();


        mBinding.lv10BtnTrue.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                mBinding.lv10BtnTrue.setBackgroundColor(Color.BLACK);
                mBinding.lv10BtnTrue.setTextColor(Color.WHITE);
            } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                mBinding.lv10BtnTrue.setBackgroundColor(Color.WHITE);
                mBinding.lv10BtnTrue.setTextColor(Color.BLACK);
                mBinding.lv10BtnTrue.callOnClick();
            }
            return true;
        });

        mBinding.lv10BtnFalse.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                mBinding.lv10BtnFalse.setBackgroundColor(Color.BLACK);
                mBinding.lv10BtnFalse.setTextColor(Color.WHITE);
            } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                mBinding.lv10BtnFalse.setBackgroundColor(Color.WHITE);
                mBinding.lv10BtnFalse.setTextColor(Color.BLACK);
                mBinding.lv10BtnFalse.callOnClick();
            }
            return true;
        });


    }

    private void setQuiz() {

        mSoal.add(new Level10Model(false, "The Great Wall of China is the only human-made object you can see from space."));
        mSoal.add(new Level10Model(false, "The sun is yellow."));
        mSoal.add(new Level10Model(true, "Tug-of-war was once an Olympic sport."));
        mSoal.add(new Level10Model(true, "Dracula was a real-life figure in history."));
        mSoal.add(new Level10Model(false, "In the famous novel of the same name, Frankenstein is a monster constructed from body parts."));
        mSoal.add(new Level10Model(true, "Edward in the Twilight series is a \"vegetarian vampire,\" meaning he drinks only non-human, animal blood."));
        mSoal.add(new Level10Model(false, "The tiniest bones in the human body are found in the hand."));
        mSoal.add(new Level10Model(true, "The deadliest weapon of World War I was the machine gun."));
        mSoal.add(new Level10Model(true, "TheRussia has the largest area of any country in the world."));
        mSoal.add(new Level10Model(true, "The mosquito has caused more human deaths than any other creature in history."));
        mSoal.add(new Level10Model(true, "The tango originated in Argentina."));
        mSoal.add(new Level10Model(false, "King Tut was one of the longest-reigning kings of ancient Egypt."));
        mSoal.add(new Level10Model(false, "Spiders belong to the \"insect\" class of animals."));
        mSoal.add(new Level10Model(true, "Volleyball was invented as a game for businessmen."));
        mSoal.add(new Level10Model(true, "A straight line measure 180 degrees."));
        mSoal.add(new Level10Model(true, "The Statue of Liberty was a gift from France to America."));
        mSoal.add(new Level10Model(true, "Ozone is \"good\" in the upper atmosphere but \"bad\" in the lower."));
        mSoal.add(new Level10Model(false, "Kryptonite is the source of Superman's power."));
        mSoal.add(new Level10Model(true, "The \"Forbidden City\" is in Beijing."));
        mSoal.add(new Level10Model(true, "Hurricanes and typhoons are the same thing."));
        mSoal.add(new Level10Model(true, "The thigh bone (femur) is the largest bone in the human body."));
        mSoal.add(new Level10Model(false, "An American was the first man in space."));

        //diacak 2x
        Collections.shuffle(mSoal);
        Collections.shuffle(mSoal);
    }

    private void setupLevel1() {
        mModel = mSoal.get(0);

        if (start_screen.isTrue) {
            Toast.makeText(this, "Thanks to bit.ly/2Hrs4I6", Toast.LENGTH_SHORT).show();
            mBinding.lv10BtnFalse.setEnabled(true);
            mBinding.lv10BtnTrue.setEnabled(true);

            if (mModel.isTrue()) {
                mBinding.lv10BtnTrue.setOnClickListener(this);
                mBinding.lv10BtnFalse.setOnClickListener(this);
            } else {
                mBinding.lv10BtnTrue.setOnClickListener(this);
                mBinding.lv10BtnFalse.setOnClickListener(this);
            }

        } else {
            String s = " tap more";
            int gg = 50 - start_screen.totalTap;
            mBinding.lv10Soal5TxtGoal.setText(gg + s);
            mBinding.lv10BtnFalse.setEnabled(false);
            mBinding.lv10BtnTrue.setEnabled(false);
        }

        mBinding.lv10TxtSoal1.setText(mModel.getSoal());

    }

    private void benar() {
        Toast.makeText(this, "Correct!", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this, Level10Soal2Activity.class));
        finish();
    }

    private void salah() {
//        Toast.makeText(this, "Reset Hapus Shared + Intent", Toast.LENGTH_SHORT).show();

        //Hapus SharedPreferer nya
        startActivity(new Intent(this, ResetActivity10.class));
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lv10_btnTrue:
                if (mModel.isTrue()) {
                    benar();
                } else {
                    salah();
                }
                break;

            case R.id.lv10_btnFalse:
                if (mModel.isTrue()) {
                    salah();
                } else {
                    benar();
                }
        }
    }
}
