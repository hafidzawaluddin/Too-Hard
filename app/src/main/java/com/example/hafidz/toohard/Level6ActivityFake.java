package com.example.hafidz.toohard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import com.example.hafidz.toohard.databinding.ActivityLevel6FakeBinding;

import java.util.ArrayList;
import java.util.Collections;

public class Level6ActivityFake extends AppCompatActivity {

    ActivityLevel6FakeBinding mBinding;
    public static Activity l6af;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_level6_fake);
        hide_and_fullscreen();
        l6af = this;
        clikListener();
        randomButton();
    }

    void intent() {
        startActivity(new Intent(this, Level6ActivityFake.class));
        overridePendingTransition(0,0);
    }

    void clikListener() {
        mBinding.FakeLv6Btn1.setOnClickListener((v) -> {
            intent();
        });

        mBinding.FakeLv6Btn2.setOnClickListener((v) -> {
            intent();
        });

        mBinding.FakeLv6Btn3.setOnClickListener((v) -> {
            intent();
        });

        mBinding.FakeLv6Btn4.setOnClickListener((v) -> {
            intent();
        });
    }

    private void hide_and_fullscreen() {
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void randomButton() {
        ArrayList<Integer> mDoorList = new ArrayList<>();
        mDoorList.add(0, R.drawable.door_percaya_diri);
        mDoorList.add(1, R.drawable.door_kuning);
        mDoorList.add(2, R.drawable.door_ungu);
        mDoorList.add(3, R.drawable.door_berani);
        Collections.shuffle(mDoorList);

        mBinding.FakeLv6Btn1.setImageResource(mDoorList.get(0));
        mBinding.FakeLv6Btn2.setImageResource(mDoorList.get(1));
        mBinding.FakeLv6Btn3.setImageResource(mDoorList.get(2));
        mBinding.FakeLv6Btn4.setImageResource(mDoorList.get(3));
    }

}
