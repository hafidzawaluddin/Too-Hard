package com.example.hafidz.toohard;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestFutureTarget;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
public class Level5Activity extends AppCompatActivity {

    private boolean mStopHandler = false;
    private Handler mHandler = new Handler();
    ImageView iv;


    SharedPreferences sharedpreferences;
    ImageView gifImageView;
    VideoView vv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level5);
        hide_and_fullscreen();
        gifImageView = findViewById(R.id.gifImageVIew);
//        gifImageView.startAnimation();
//        final GifDrawable[] gd = new GifDrawable[1];
        Glide.with(this)
                .load(R.drawable.colokan)
                .into(gifImageView);
        Uri video = Uri.parse("android.resource://com.example.hafidz.toohard/raw/video");
        vv = findViewById(R.id.VidVi);
        vv.setVideoURI(video);
        vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                int level = sharedpreferences.getInt("lastlevel", 1);
                if (level <= 5) {
                    editor.putInt("lastlevel", 6);
                    editor.commit();
                }
                startActivity(new Intent(Level5Activity.this, Level6Splash.class));
                finish();
                overridePendingTransition(0, 0);

            }
        });
        vv.setVisibility(View.INVISIBLE);


        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                // do your stuff - don't create a new runnable here!
                if (!mStopHandler) {
                    mHandler.postDelayed(this, 3000);

                    if (isConnected(Level5Activity.this)) {
                        mStopHandler = true;
                        Log.wtf("run: ", "handler");
//                        imageView.setVisibility(View.INVISIBLE);
//                        gifImageView.setVisibility(View.VISIBLE);
//                        gifImageView.startAnimatx ion(
                        vv.setVisibility(View.VISIBLE);
                        gifImageView.setVisibility(View.INVISIBLE);

                            vv.start();

//                        gifImageView.setOnAnimationStop(new GifImageView.OnAnimationStop() {
//                            @Override
//                            public void onAnimationStop() {
//                                Log.wtf("onAnimationStop: ", "onStop");
//                                startActivity(new Intent(Level5Activity.this, Level6Activity.class));
//                                finish();
//                            }
//                        });

                    }
                }
            }
        };
        mHandler.post(runnable);
    }

    public static boolean isConnected(Context context) {
        Log.wtf("isConnected: ", "true");
        Intent intent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        return plugged == BatteryManager.BATTERY_PLUGGED_AC || plugged == BatteryManager.BATTERY_PLUGGED_USB;
    }

    private void hide_and_fullscreen() {
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        gifImageView.stopAnimation();
    }
}
