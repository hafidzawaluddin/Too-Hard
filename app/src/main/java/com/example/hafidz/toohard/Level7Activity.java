package com.example.hafidz.toohard;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.hafidz.toohard.databinding.ActivityLevel7Binding;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import ir.samanjafari.easycountdowntimer.CountDownInterface;

public class Level7Activity extends AppCompatActivity implements CountDownInterface {

    //    private int time = 50000;
    private int delay = 500;
    private int totalatas = 50;
    private int totalkiri = 100;
    private int totalkanan = 30;
    private int totalbawah = 75;
    private ActivityLevel7Binding mBinding;
    private Handler mHandler = new Handler();
    private Runnable mRunnable1, mRunnable2, mRunnable3, mRunnable4;

    private Clicked buttonNow;
    private int levelnow = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hide_and_fullscreen();
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_level7);
        setOnclick();
        lv1();
        setRunnable();
    }


    private void setOnclick() {
        mBinding.lv7BtnUp.setOnClickListener((v) -> {
            klikTombol(Clicked.ATAS);
        });

        mBinding.lv7BtnKiri.setOnClickListener((v) -> {
            klikTombol(Clicked.KIRI);
        });

        mBinding.lv7BtnKanan.setOnClickListener((v) -> {
            klikTombol(Clicked.KANAN);
        });
        mBinding.lv7BtnDown.setOnClickListener(v -> {
            klikTombol(Clicked.BAWAH);
        });
    }

    private void klikTombol(Clicked click) {
        buttonNow = click;
        switch (click) {
            case ATAS:
                if (levelnow != 1) {
                    reset();
                } else if (totalatas <= 1) {
//                    onFinish();
                    lvl2();
                    levelnow = 2;
                } else {
                    totalatas--;
                    setText(totalatas);
                }
                break;
            case KIRI:
                if (levelnow != 2) {
                    reset();
                } else if (totalkiri <= 1) {
//                    onFinish();
                    lvl3();
                    levelnow = 3;
                } else {
                    totalkiri--;
                    setText(totalkiri);
                }
                break;
            case KANAN:
                if (levelnow != 3) {
                    reset();
                } else if (totalkanan <= 1) {
//                    onFinish();
                    lvl4();
                    levelnow = 4;
                } else {
                    totalkanan--;
                    setText(totalkanan);
                }
                break;
            case BAWAH:
                if (levelnow != 4) {
                    reset();
                } else if (totalbawah <= 1) {
                    SharedPreferences sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    int level = sharedpreferences.getInt("lastlevel", 1);
                    if (level <= 7) {
                        editor.putInt("lastlevel", 8);
                        editor.commit();
                    }
                    startActivity(new Intent(this, Level8Splash.class));
                    finish();
                    overridePendingTransition(0, 0);
                } else {
                    totalbawah--;
                    setText(totalbawah);
                }
                break;
            default:
                break;
        }
    }

    private void reset() {
        totalatas = 50;
        totalkiri = 100;
        totalkanan = 30;
        totalbawah = 75;

        setText(totalatas);
        mBinding.lv7ImgCommand.setImageResource(R.drawable.atas);
        levelnow = 1;
    }

    private void hide_and_fullscreen() {
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void randomLevel() {
        int rand = new Random().nextInt(5) + 1;

        switch (rand) {
            case 1:
                lvl2();
                break;
            case 2:
                lvl3();
                break;
            case 3:
                lvl3();
                break;
            case 4:
                lvl4();
                break;
            case 5:
                lvl5();
                break;
        }
    }

    private void lv1() {
        totalatas = 50;
        setText(50);
        setTimer(20);
    }

    private void lvl2() {
//        mTimer = null;
        setTimer(40);
        totalkiri = 100;
        setText(totalkiri);
        mBinding.lv7ImgCommand.setImageResource(R.drawable.kiri);
    }

    private void lvl3() {
        totalkanan = 30;
        setTimer(8);
        setText(totalkanan);
        mBinding.lv7ImgCommand.setImageResource(R.drawable.kanan);
    }

    private void lvl4() {
        totalbawah = 75;
        setTimer(20);
        setText(totalbawah);
        mBinding.lv7ImgCommand.setImageResource(R.drawable.bawah);
    }

    private void lvl5() {
    }

    private void setText(int total) {
        mBinding.lv7TxtCommand.setText("" + total);
    }

    private void setRunnable() {
        mRunnable1 = () -> lvl2();
        mRunnable2 = () -> lvl3();
        mRunnable3 = () -> lvl4();
        mRunnable4 = () -> lvl5();
    }

    private void setTimer(int time) {
        mBinding.lv7Timer.setTime(0, 0, time);

        mBinding.lv7Timer.setOnTick(this);
    }

    @Override
    public void onTick(long time) {

    }

    @Override
    public void onFinish() {
//        Toast.makeText(this, "Watchout!", Toast.LENGTH_SHORT).show();
        mBinding.lv7BtnUp.setOnClickListener(null);
        mBinding.lv7BtnDown.setOnClickListener(null);
        mBinding.lv7BtnKanan.setOnClickListener(null);
        mBinding.lv7BtnKiri.setOnClickListener(null);
    }

    enum Clicked {
        KIRI, KANAN, ATAS, BAWAH
    }
}
