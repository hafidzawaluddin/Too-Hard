package com.example.hafidz.toohard;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;

import java.io.IOException;

public class Level4Activity extends AppCompatActivity implements SensorEventListener{

    boolean mStopHandler;
    Handler mHandler = new Handler();
    float eaea = 10;
    SensorManager sensorManager;
    Sensor sensor;
    MediaPlayer mediaPlayer;
    Vibrator vibrator;
    SharedPreferences sharedpreferences;
    Runnable runnable;
    int mute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level4);
        hide_and_fullscreen();
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        mute = sharedpreferences.getInt("mute", 0);
        if (mute == 0){
            ring();
        }
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        mStopHandler = false;
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        runnable = new Runnable() {
            @Override
            public void run() {
                if (!mStopHandler) {
                    vibrator.vibrate(500);
                    mHandler.postDelayed(this, 1000);
                    if (eaea <= 1){
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        int level = sharedpreferences.getInt("lastlevel", 1);
                        if (level <= 4) {
                            editor.putInt("lastlevel", 5);
                            editor.commit();
                        }
                        Intent i = new Intent(Level4Activity.this, Level5Splash.class);
                        startActivity(i);
                        mStopHandler = true;
                        mediaPlayer.stop();
                        finish();
                        overridePendingTransition(0, 0);
                    }
                }
            }
        };

        mHandler.post(runnable);
    }

    private void hide_and_fullscreen() {
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_PROXIMITY){
            eaea = event.values[0];
            Log.wtf("onSensorChanged: ", ""+eaea);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public void ring(){
        try {
            Resources resources = getResources();
            AssetFileDescriptor assetFileDescriptor = resources.openRawResourceFd(R.raw.phone);
            mediaPlayer = MediaPlayer.create(this, R.raw.phone);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_VOICE_CALL);
            mediaPlayer.reset();
            mediaPlayer.setDataSource(assetFileDescriptor.getFileDescriptor(), assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength());
            mediaPlayer.setLooping(true);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mute == 0) {
            mediaPlayer.stop();
        }
        mHandler.removeCallbacks(runnable);
    }
}
