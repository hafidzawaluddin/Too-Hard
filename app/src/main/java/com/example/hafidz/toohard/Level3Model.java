package com.example.hafidz.toohard;

/**
 * Created by Hafidz on 04/01/2018.
 */

public class Level3Model {
    private String text;
    private boolean isTrue;

    Level3Model(String text, boolean isTrue) {
        this.text = text;
        this.isTrue = isTrue;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isTrue() {
        return isTrue;
    }

    public void setTrue(boolean aTrue) {
        isTrue = aTrue;
    }
}
