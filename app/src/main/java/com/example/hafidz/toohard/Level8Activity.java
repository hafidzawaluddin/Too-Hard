package com.example.hafidz.toohard;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.PointF;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.example.hafidz.toohard.databinding.ActivityLevel8Binding;


public class Level8Activity extends AppCompatActivity implements QRCodeReaderView.OnQRCodeReadListener {

    private ActivityLevel8Binding mBinding;
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_level8);

        hide_and_fullscreen();
        onClick();
        setUpCamera();
    }

    private void setUpCamera() {
        mBinding.lv8QrCode.setOnQRCodeReadListener(this);
        mBinding.lv8QrCode.setQRDecodingEnabled(true);
        mBinding.lv8QrCode.setAutofocusInterval(1000L);
        mBinding.lv8QrCode.setTorchEnabled(true);
        mBinding.lv8QrCode.setBackCamera();

    }

    private void onClick() {
        mBinding.lv8ButtonOk.setOnClickListener((v) -> {
            String result = mBinding.lv8EditText.getText().toString();
            if (result.equals("Finish The Game")) {
                sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                int level = sharedpreferences.getInt("lastlevel", 1);
                if (level <= 8) {
                    editor.putInt("lastlevel", 9);
                    editor.commit();
                }
                startActivity(new Intent(this, Level9Splash.class));
                overridePendingTransition(0, 0);
            } else {
                Toast.makeText(this, "Invalid Key", Toast.LENGTH_SHORT).show();
            }
        });

        mBinding.lv8TxtCode.setOnClickListener((v) -> {
            Toast.makeText(this, "GoodLuck", Toast.LENGTH_SHORT).show();
            String url = "https://ibb.co/di9aww";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
//            Log.wtf("onClick: ", "https://ibb.co/di9aww");
        });
    }

    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        Log.wtf("onQRCodeRead: ", text);
        mBinding.lv8TxtCode.setText(text);
//        mBinding.lv8TxtCode.setOnClickListener((v)-> Log.wtf("onQRCodeRead: ", "GG"));
        mBinding.lv8QrCode.stopCamera();
        mBinding.lv8QrCode.setVisibility(View.GONE);
        Toast.makeText(this, "Scanned", Toast.LENGTH_SHORT).show();
    }

    private void hide_and_fullscreen() {
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

}
