package com.example.hafidz.toohard;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.hafidz.toohard.databinding.ActivityLevel10Soal3Binding;

public class Level10Soal3Activity extends AppCompatActivity {

    private ActivityLevel10Soal3Binding mBinding;
    private Level10Model mModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_level10_soal3);

        mModel = Level10Activity.mSoal.get(2);
        setupLevel3();
        mBinding.lv10Soal3BtnTrue.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    mBinding.lv10Soal3BtnTrue.setBackgroundColor(Color.BLACK);
                    mBinding.lv10Soal3BtnTrue.setTextColor(Color.WHITE);
                }
                else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    mBinding.lv10Soal3BtnTrue.setBackgroundColor(Color.WHITE);
                    mBinding.lv10Soal3BtnTrue.setTextColor(Color.BLACK);
                    mBinding.lv10Soal3BtnTrue.callOnClick();
                }
                return true;
            }
        });
        mBinding.lv10Soal3BtnFalse.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    mBinding.lv10Soal3BtnFalse.setBackgroundColor(Color.BLACK);
                    mBinding.lv10Soal3BtnFalse.setTextColor(Color.WHITE);
                }
                else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    mBinding.lv10Soal3BtnFalse.setBackgroundColor(Color.WHITE);
                    mBinding.lv10Soal3BtnFalse.setTextColor(Color.BLACK);
                    mBinding.lv10Soal3BtnFalse.callOnClick();
                }
                return true;
            }
        });
    }

    private void setupLevel3() {

        if (start_screen.isTrue) {
            mBinding.lv10Soal3BtnTrue.setEnabled(true);
            mBinding.lv10Soal3BtnFalse.setEnabled(true);
        } else {
            mBinding.lv10Soal3BtnTrue.setEnabled(false);
            mBinding.lv10Soal3BtnFalse.setEnabled(false);
        }

        mBinding.lv10Soal3TxtSoal3.setText(mModel.getSoal());

        if (mModel.isTrue()) {
            mBinding.lv10Soal3BtnTrue.setOnClickListener(view -> benar());
            mBinding.lv10Soal3BtnFalse.setOnClickListener(view -> salah());
        } else {
            mBinding.lv10Soal3BtnTrue.setOnClickListener(view -> salah());
            mBinding.lv10Soal3BtnFalse.setOnClickListener(view -> benar());
        }
    }

    private void benar() {
        Toast.makeText(this, "Correct", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this, Level10Soal4Activity.class));
        finish();
    }

    private void salah() {
//        Toast.makeText(this, "Reset Hapus Shared + Intent", Toast.LENGTH_SHORT).show();

        //Hapus SharedPreferer nya
        startActivity(new Intent(this, ResetActivity10.class));
        finish();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Level10Activity.mActivity.finish();
    }
}
