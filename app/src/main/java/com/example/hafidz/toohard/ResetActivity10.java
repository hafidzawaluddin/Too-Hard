package com.example.hafidz.toohard;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.hanks.htextview.fade.FadeTextView;

public class ResetActivity10 extends AppCompatActivity {

    FadeTextView level;
    private static int time = 4000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_reset10);

        level = (FadeTextView) findViewById(R.id.tv_level);

        final int[] i = {0};
        String[] kalimat = {"We just don't want you to win this game","So we should reset this game","Because you select the wrong answer","Bye Bye! =))"};

        level.setSoundEffectsEnabled(false);
        level.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (i[0] <= 3) {
                    ((FadeTextView) view).animateText(kalimat[i[0]]);
                    ++i[0];
                }
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPreferences sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putInt("lastlevel", 1);
                editor.commit();
                start_screen.level = 1;
                startActivity(new Intent(ResetActivity10.this, Level1Notification.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                finish();
                overridePendingTransition(0, 0);
            }
        }, 7500);
        Handler handler = new Handler();
        Runnable mRunnable = new Runnable() {
            @Override
            public void run() {
                level.performClick();
            }
        };

//1000 di delay 1 detik
        handler.postDelayed(mRunnable, 200);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                level.performClick();
            }
        },2200);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                level.performClick();
            }
        },4200);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                level.performClick();
            }
        },6200);
    }
}
