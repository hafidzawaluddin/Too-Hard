package com.example.hafidz.toohard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

import com.example.hafidz.toohard.databinding.ActivityLevel6Binding;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;

public class Level6Activity extends AppCompatActivity {

    ActivityLevel6Binding mBinding;
    int benar;
    boolean btn1, btn2, btn3, btn4;
    public static Activity lv6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_level6);
        hide_and_fullscreen();

        lv6 = this;
        Random rand = new Random();
        benar = rand.nextInt(4) + 1;
        Log.wtf("onCreate: ", "" + benar);

        switch (benar) {
            case 1:
                btn1 = true;
                mBinding.BaseLv6Btn1.setImageResource(R.drawable.door_berani);
                setDoorColor(1);
                break;
            case 2:
                btn2 = true;
                mBinding.BaseLv6Btn2.setImageResource(R.drawable.door_berani);
                setDoorColor(2);
                break;
            case 3:
                btn3 = true;
                mBinding.BaseLv6Btn3.setImageResource(R.drawable.door_berani);
                setDoorColor(3);
                break;
            case 4:
                btn4 = true;
                mBinding.BaseLv6Btn4.setImageResource(R.drawable.door_berani);
                setDoorColor(4);
                break;
        }
        clikListener();
    }

    void salah() {
        startActivity(new Intent(this, Level6ActivityFake.class));
        overridePendingTransition(0,0);
    }

    void benar() {
        startActivity(new Intent(this, Level6ActivityTrue.class));
        overridePendingTransition(0,0);
    }

    void clikListener() {
        mBinding.BaseLv6Btn1.setOnClickListener((v) -> {
            if (btn1) {
                benar();
            } else {
                salah();
            }
        });

        mBinding.BaseLv6Btn2.setOnClickListener((v) -> {
            if (btn2) {
                benar();
            } else {
                salah();
            }
        });

        mBinding.BaseLv6Btn3.setOnClickListener((v) -> {
            if (btn3) {
                benar();
            } else {
                salah();
            }
        });

        mBinding.BaseLv6Btn4.setOnClickListener((v) -> {
            if (btn4) {
                benar();
            } else {
                salah();
            }
        });
    }

    private void hide_and_fullscreen() {
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void setDoorColor(int i) {

        ArrayList<Integer> mDoorList = new ArrayList<>();
        mDoorList.add(0, R.drawable.door_percaya_diri);
        mDoorList.add(1, R.drawable.door_kuning);
        mDoorList.add(2, R.drawable.door_ungu);
        Collections.shuffle(mDoorList);


        switch (i) {
            case 1:
                mBinding.BaseLv6Btn2.setImageResource(mDoorList.get(0));
                mBinding.BaseLv6Btn3.setImageResource(mDoorList.get(1));
                mBinding.BaseLv6Btn4.setImageResource(mDoorList.get(2));
                break;
            case 2:
                mBinding.BaseLv6Btn1.setImageResource(mDoorList.get(0));
                mBinding.BaseLv6Btn3.setImageResource(mDoorList.get(1));
                mBinding.BaseLv6Btn4.setImageResource(mDoorList.get(2));
                break;
            case 3:
                mBinding.BaseLv6Btn1.setImageResource(mDoorList.get(0));
                mBinding.BaseLv6Btn2.setImageResource(mDoorList.get(1));
                mBinding.BaseLv6Btn4.setImageResource(mDoorList.get(2));
                break;
            case 4:
                mBinding.BaseLv6Btn1.setImageResource(mDoorList.get(0));
                mBinding.BaseLv6Btn2.setImageResource(mDoorList.get(1));
                mBinding.BaseLv6Btn3.setImageResource(mDoorList.get(2));
                break;
        }
    }
}

