package com.example.hafidz.toohard;

/**
 * Created by Hafidz on 27/01/2018.
 */

public class Level10Model {
    private boolean isTrue;
    private String soal;

    public boolean isTrue() {
        return isTrue;
    }

    public String getSoal() {
        return soal;
    }

    public Level10Model(boolean isTrue, String soal) {
        this.isTrue = isTrue;
        this.soal = soal;
    }
}
